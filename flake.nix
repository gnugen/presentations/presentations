{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11";
  };

  outputs = { self, nixpkgs }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      utils = pkgs.callPackage ./utils.nix {};
    in {
      packages.x86_64-linux = {
        hands-on-git = utils.copyDir "hands-on-git" ./hands-on-git;

        devShell.x86_64-linux = pkgs.mkShell {
          buildInputs = utils.all-build-tools;
        };
      };

      # a tiny webserver for local development
      defaultApp.x86_64-linux = {
        type = "app";
        program = "${pkgs.x86_64-linux.darkhttpd}/bin/darkhttpd ${self.defaultPackage.x86_64-linux} --ipv6 --addr ::1";
      }

    };
}
