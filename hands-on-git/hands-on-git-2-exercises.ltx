\documentclass[a4paper]{article}
\usepackage{fourier-otf}
\usepackage[margin=2cm]{geometry}
\usepackage{exsheets}
\usepackage{svg}

\title{\vspace{-3em}Intermediate Git Workshop Exercises}
\author{Lucas Crijns and Antoine Fontaine}
\date{April 2022}
\begin{document}

\maketitle

\section{Introduction}
In these exercises you can get the hang of all the git commands that have just been highlighted. To start off, clone the exercise repository: \\
\texttt{git clone https://gitlab.gnugen.ch/crijns/intermediate-git-workshop.git} \\
This repository contains a basic book on how git works with \texttt{.txt} files in the \texttt{chapters/} directory and a \texttt{main.py} file to assemble the book.
You will automatically be put in the \textbf{main}-branch.
For reference, the history/graph of this repository looks like:

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{Diagram.png}
\end{figure}


\section{Exercises}
\subsection{Switching}
You are currently working in the \textbf{clone-chapter} branch. Your friends in this project have been working on the branches \textbf{init-chapter} and \textbf{commit-chapter}, where they are trying to describe the respective git commands. 
Therefore, switch to the \textbf{clone-chapter} branch, your \textbf{personal branch}.

\begin{question}
\begin{enumerate}
    \item Make some changes in \texttt{chapters/clone.txt}.
    \item After you have done that, switch to the branch \textbf{init-chapter} to check the work of you friend. However, you are not yet finished with editing the file \texttt{chapters/clone.txt}, so you do not commit yet.
    \item Inspect some of the files in the branch. 
    \item Go back to your \textbf{personal branch}
    \item Get your uncomitted changes back.
    \item Make a commit of your changes
\end{enumerate}
\end{question}
\textbf{Hint}: Use the \texttt{git stash} command to do this.
\begin{solution}
\begin{enumerate}
    \item git stash $\quad$ Temporarily stores your current modifications somewhere else
    \item git switch init-chapter
    \item git switch - $\quad$ Switch back to the previous branch
    \item git stash pop $\quad$ Retrieve your previsously temporarily stored modifications
    \item git add chapters/clone.txt
    \item git commit -m "Some message"
\end{enumerate}
\end{solution}

\subsection{Merging}
It's now finally time to bring together all your chapters and finish the book. To do so, we have to merge all the branches into \textbf{main}. We will merge in the following order: \textbf{clone-chapter, commit-chapter, init-chapter}.
\begin{question}
We will start with the branch \textbf{clone-chapter}, your \textbf{personal branch}. As you can see from the history, (try running \texttt{tig --all}), it comes directly from branch \textbf{main} and we added 2 commits. If we merge this branch into \textbf{main}, we will do a so-called fast-forward merge.
For this, first switch to branch \textbf{main} and then do the merge.
\end{question}
\textbf{Hint:} Use the \texttt{git merge <branch>} command.
\begin{solution}
\begin{enumerate}
    \item git switch main
    \item git merge clone-chapter $\quad$ Merges the clone-chapter branch into the main branch
\end{enumerate}
\end{solution}

\begin{question}
Now that we have merged \textbf{clone-chapter}, let's merge the \textbf{commit-chapter} into the \textbf{main} branch. This merge will not give a conflict, because separate files have been changed or added.
\end{question}
\begin{solution}
git merge commit-chapter $\quad$ Merges the commit-chapter branch into the main branch
\end{solution}

\begin{question}
After all these merges, we are now ready to merge the changes from the \textbf{init-chapter} branch into the \textbf{main} branch. However, in this branch a file \texttt{chapters/clone.txt}. Unfortunately, your already added this in the \textbf{clone-chapter} branch. Hence, merging this will give a conflict. 
\begin{enumerate}
    \item Merge the changes from the \textbf{init-chapter} into the \textbf{main} branch.
    \item Resolve the conflict.
    \item Commit the final result.
\end{enumerate}
\end{question}
\begin{solution}
\begin{enumerate}
    \item git merge init-chapter $\quad$ Initiates a merge from the init-chapter branch into the main branch
    \item Fix chapters/clone.txt $\quad$ Since there is a conflict, you have to resolve this by manually editing the file.
    \item git add . $\quad$ Mark all files that have been changed ready for commit. The `.' specifies this.
    \item git commit
\end{enumerate}
\end{solution}

\subsection{History}
Now let's explore some history. 
\begin{question}
Go back in time to the commit with message \textbf{``Add book assembly script"}. Check how the repository changes and look around. After this, switch back to the \textbf{main} branch.
\end{question}
\begin{solution}
\begin{enumerate}
    \item git checkout dbc43da32ae3ce47034e7add4efec2c56149a107 $\quad$ Switch to this specific commit. The hash has been obtained using \texttt{tig --all}
    \item git switch - $\quad$ Switch back to the branch you came from
\end{enumerate}
\end{solution}
\textbf{Hint:} Use either \texttt{git log} or \texttt{tig --all} to find the commit hash. Then go back in time using \texttt{git checkout <commit-hash>}.

\begin{question}
How long ago did you merge the branches in the previous exercises? Try to go back in time to that moment and check where git takes you. Then go back to the \textbf{main} branch.
\end{question}
\textbf{Hint}: Remember that \texttt{git checkout <moment in time>} takes moments in times of the form \texttt{@\{x minutes ago\}}.
\begin{solution}
\begin{enumerate}
    \item git checkout @\{20 minutes ago\} $\quad$ Switch to the commit that was closest to 20 minutes time ago.
    \item git switch - $\quad$ Switch back to the branch you came from
\end{enumerate}
\end{solution}

\subsection{Reverting}
As you might have noticed in the log, there is a bug in "introduction.txt". We want to rectify this by reverting this commit.
\begin{question}
Revert the commit with message \textbf{``INTODUCED BUG in introduction.txt''}. Do this revert from the \textbf{main} branch. Notice in the log how git made a new commit that reverts the aforementioned commit.
\end{question}
\textbf{Hint:} Use the \texttt{git revert <hash of commit>} command to revert the commit.
\begin{solution}
git revert 940d19302e052b59c4db11db7a1fc73c4d77d366 $\quad$ This command makes a new commit by removing the changes that were introduced by this commit. The commit hash has been found by using \texttt{tig --all}.
\end{solution}

\subsection{Advanced}
If you are up for it, you can try some more advanced stuff like \texttt{git bisect}. Here we will try it out on the workshop repository to see if we can pinpoint the commit that introduced the just reverted bug, because normally it is not so clearly indicated whether the commit introduced a bug. 

For this to work with the just reverted commit, go back one commit in history, just before the revert. A quick way to do this is with: \\
\texttt{git checkout @\^{}} or using \texttt{git checkout HEAD\^{}} \\
In the above the `\texttt{@}'-character is synonymous with `HEAD', which is a pointer to where we currently are, namely the revert. The caret `\^{}' indicates that we go to the `parent' commit, so one commit back in time. 
\begin{question}
Now that we are ready to start bisecting, issue \texttt{git bisect start}. This marks the current commit as a `bad' commit, since we have a bug here. Then go back in time to the commit with message \textbf{``Initial commit''}. Mark this commit as `good'. \\
Once you have done this, git will automatically start the process and take you across history. Each time you have to mark a commit as `good' or `bad', respectively. To determine this, the bug introduced has the content \texttt{BUG!} written in \texttt{chapters/introduction.txt}. If you see this, it was `bad' and if you don't then it was `good'. In the end you will find that we end up with the commit that was reverted above.
\end{question}
\textbf{Hint:} Use \texttt{git bisect good} and \texttt{git bisect bad} to mark a commit as `good' or `bad', respectively.
\begin{solution}
\begin{enumerate}
    \item \texttt{git checkout @\^{}} $\quad$ Switch to the commit before the current commit
    \item git bisect start $\quad$ Start the bisecting process
    \item git bisect bad $\quad$ Marks a commit as `bad'
    \item git checkout 767f9514c65115d213f51929aa2c1e749c76f381 $\quad$ Switch to the initial commit. The hash can be obtained from \texttt{tig --all}.
    \item git bisect good $\quad$ Marks a commit as `good'
    \item Now the bisecting process starts and you can check cat chapters/introduction.txt for the bug and mark then good or bad depending on the contents. 
\end{enumerate}
\end{solution}

\newpage
\section{Solutions}
\printsolutions
\end{document}
