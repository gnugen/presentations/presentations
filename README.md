# Presentations

The content of this repository is licensed under the *Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International License*.

![CC-BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)
