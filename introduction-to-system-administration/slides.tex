\documentclass{beamer}
\usetheme{CambridgeUS}

\usepackage{hyperref}
\usepackage{multicol}

\title{Introduction to System Administration}
\author{Timothée Floure}
\institute{GNU Generation}
\date{2019-05-02}
\subject{Computer Science}

\begin{document}
  \frame{\titlepage}
  \begin{frame}
    \begin{center}
      \includegraphics[width=12cm]{devotion_to_duty.png}
    \end{center}
  \end{frame}

  \begin{frame}
    \frametitle{Disclaimer}

    There is ways too much text on these slides but it should help you if you
    review them offline. You'll get some drawings on the blackboard. \\\

    You won't get detailed configuration files, as you should dig in by
    yourself. (Also, time). I quickly show them during the live example and
    would be happy to discuss them afterwards.
  \end{frame}

  \begin{frame}
    \frametitle{What, Why?}

    \begin{itemize}
      \item A system administrator (AKA {\it sysadmin}) designs and takes cares
        of systems, mostly servers.
      \item You need someone to deploy and maintain the various IT services you
        use such as your mail server, file-sharing tool or even networking.
      \item Developers wants their applications to work but usually does not
        know how or want to handle the underlying infrastructure.
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{What makes a sysadmin?}

    \begin{itemize}
      \item IT common sense! There are 'obvious' concepts to follow, although
        you mostly gain this through experience.
      \item Looks for documentation, and {\bf read} it. Manpages are your friends!
    \end{itemize}
  \end{frame}

  \section{The basics}
  \begin{frame}
    \frametitle{You need something to work with: Hardware}

    You can easily (and cheaply) rent a server from a cloud provider:

    \begin{itemize}
      \item {\it VPS} or {\it Virtual Server}: a virtual machine hosted on huge
        (shared) physical server. Very flexible as you can scale it in a few
        seconds.
      \item {\it Baremetal} or {\it Dedicated server}: physical node 100\% dedicated to
        you, usually more powerful but less flexible and more expensive.
    \end{itemize}
  \end{frame}
  \begin{frame}
    \frametitle{You need something to work with: Hardware}

    Some providers:

    \begin{itemize}
      \item Exoscale (Expensive but high-quality swiss provider)
      \item OVH (very large european provider)
      \item Scaleway (cheap european provider)
      \item DigitalOcean (very large US provider with datacenters all across the world)
      \item Liteserver (Netherlands, super cheap for low-end nodes)
      \item ...
    \end{itemize}

  \end{frame}

  \begin{frame}
    \frametitle{You need something to work with: Software}

    You need an operating system: most common are GNU/Linux distributions but
    some *BSD can be seen. Microsoft's Windows Server doesn't count. \\\

    You want a stable distribution with long-term support (you don't want major
    upgrades every 6 months).

    \begin{itemize}
      \item Debian stable: what we use here.
      \item CentOS (and RHEL)
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{Things to keep in mind}

    \begin{itemize}
      \item Security: you don't want your whole infrastructure to be
        compromised if a single service is vulnerable. Multiple layers of
        security and least privileges.
      \item Maintainability: many forget that most of the cost lies in
        maintenance, not in the initial setup.
      \item Simplicity: complexity is your worst enemy.
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{Think before you act}

    \begin{itemize}
      \item You don't want to break what's already working.
      \item {\bf READ} the documentation.
      \item Don't do things you do not understand fully.
      \item It might not be easy to go back and you will likely 'pollute' your
        system. You will iterate a few time in the design of your
        infrastructure, but it takes a lot of time!
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{Networking, or when things go wrong}

    \begin{itemize}
      \item Beware of networking: its complexity increase exponentially and
        it's quite ugly when it blows up (it will). It bites.
      \item Simple internet access won't be an issue... but might need need
        firewalls, bridges, private connections, and so on in the future.
    \end{itemize}
  \end{frame}

  \section{A fresh node}

  \begin{frame}
    \frametitle{A fresh node}

    We have a freshly installed Debian Stretch (current stable) system. Where do we start?
  \end{frame}

  \begin{frame}
    \frametitle{Networking and DNS entries}

    Basic networking (= access to and from the outside world) should be
    configured for you in: {\texttt /etc/network/interfaces}. \\\

    Runtime configuration can be done with the {\texttt ip} tool, the most
    common usage being {\texttt ip addr} to display current addresses.

    You might encounter the {\texttt ifconfig } tool but it is deprecated and
    has been replaced by {\texttt ip}. {\texttt ip link} and {\texttt ip route}
    are also used for non-trivial operations.
  \end{frame}

  \begin{frame}[fragile]
    \frametitle{Networking and DNS entries}

    You want to point a domain name to your server, which will be an {\texttt
    A} field directing to the server's IPv4 address. \\\

    Beware of DNS propagation: changes can take a few hours to be propagated
    across the world. \\\

    You can use the {\texttt dig} command to check DNS records:
    \begin{verbatim}
    ~ » dig +short A gnugen.ch
    192.26.40.10

    \end{verbatim}
  \end{frame}

  \begin{frame}[fragile]
    \frametitle{SSH access: a remote shell}

    Servers are managed using the command line, and SSH is a protocol allowing
    you to open remote shells over the network. \\\

    Assuming there already is an SSH service running on your server, you can
    join it using {\texttt ssh myuser@myserver}.

    \begin{verbatim}
    ~ » ssh floure@rainbowdash.gnugen.ch
    floure@rainbowdash.gnugen.ch's password:
    Linux rainbowdash 4.13.0-1-amd64 #1 SMP Debian [...]
    [...]
    Last login: Thu Apr 25 20:11:27 2019 from [...]
    floure@rainbowdash:~ »
    \end{verbatim}
  \end{frame}

  \begin{frame}[fragile]
    \frametitle{SSH access: a remote shell}

    A widely used security feature is to use an 'SSH keypair' (private and
    public keys) to authenticate against an SSH server and to disable
    password-based authentication. \\\

    There exist tools (such as cockpit) that hide common operation behind
    graphical tools: they can be useful but don't use them if you're not
    familiar with the command-based workflow as they hide critical operations.
  \end{frame}

  \begin{frame}
    \frametitle{SSH access: a remote shell}

    You now have a shell open on your server, you'll want to install your
    favorite tools:

    \begin{itemize}
      \item {\texttt vim} (or the much simpler {\texttt nano}) text editor
      \item {\texttt htop} as an interactive process viewer
      \item {\texttt etckeeper} to keep your configuration files versioned (in {\texttt git})
    \end{itemize}

    You can use the {\texttt update-alternatives --config editor} command to
    change the default text editor of your Debian system.
  \end{frame}

  \begin{frame}
    \frametitle{System's directory structure}

    Follows the Linux FHS (Filesystem Hierarchy Standard)
    \begin{itemize}
      \item {\texttt /etc} Host-specific system-wide configuration files.
      \item {\texttt /home} Users' home directories.
      \item {\texttt /var} Variable files.
      \item {\texttt /var/log} Log files. Various logs.
      \item {\texttt /var/lib} Persistent data modified by programs as they run,
      \item ...
    \end{itemize}

    Read \url{https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard}.
  \end{frame}

  \begin{frame}
    \frametitle{Software management}

    Distributions comes with repositories full of packages software, which are
    managed via a package manager. You missed a few steps if you did know this! \\\

    In Debian, the package format is {\texttt .deb} and the default package
    managers is {\texttt apt}. {\texttt apt-get} is low-level and you should
    not use it; {\texttt aptitude} is an alternative and offers more advanced
    features.
  \end{frame}

  \begin{frame}
    \frametitle{Software management}

    Distributions comes with repositories full of packages software, which are
    managed via a package manager. You missed a few steps if you did know this! \\\

    In Debian, the package format is {\texttt .deb} and the default package
    managers is {\texttt apt}. {\texttt apt-get} is low-level and you should
    not use it; {\texttt aptitude} is an alternative and offers more advanced
    features.
  \end{frame}

  \begin{frame}[fragile]
    \frametitle{Software management}

    \begin{itemize}
      \item First of all, you have to update the local package index: {\texttt apt
        update}.
      \item To install a package: {\texttt apt install mypackage},
      \item To upgrade your system: {\texttt apt upgrade}.
    \end{itemize}

    Rule of thumb: {\bf stick to your package manager and to the distribution's
    repositories as much as possible}.
  \end{frame}

  \begin{frame}
    \frametitle{Software management}

    You want your system to stay up to date: manual upgrades won't do. You can
    use unattended-upgrades to do so automatically (usually security updates
    only, daily).

    {\texttt apt-listchange} is also useful, as it keeps you up-to-date with
    recent changes and available updates.
  \end{frame}

  \begin{frame}
    \frametitle{Use sudo instead of working as root}

    Working in a root shell can be dangerous! You should use your own user, and
    use the {\texttt sudo} to only run specific command as root. \\\

    Sudo can also be used tu run command as any other user: {\texttt sudo -u
    someonelse whoami}.
  \end{frame}

  \begin{frame}
    \frametitle{Use sudo instead of working as root}

    \begin{itemize}
      \item {\texttt sudo} might not be installed: {\texttt apt install sudo}
      \item {\texttt sudo}'s configuration file lives in {\texttt /etc/sudoers}
      \item Create your user: {\texttt useradd myusername}
      \item Add the user to the {\texttt sudo} group: {\texttt usermod -aG sudo}
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{Version your configuration}

    The system's configuration is located under {\texttt /etc}: versioning it
    allows you to know what/who/why as well as going back in time. \\\

    {\texttt etckeeper} is a tool allowing you to easily hook a version system
    (e.g. {\texttt git}) with debian's package manager.
  \end{frame}

  \begin{frame}
    \frametitle{Firewall}

    Altrhough it is not stricly required, it's always a good idea to block
    incoming network connection to theorically unused ports. \\\

    Your serverŝ provider might propose a built-in firewalling tool
    manageblable from their web interfaces.  \\\

    Linux firewall's engine is iptables but it's low-level: there many
    convinients tools such as ufw or shorewall to generate iptables rules from
    a high-level DSL. \\
  \end{frame}

  \begin{frame}
    \frametitle{Mail delivery}

    A basic mail system is useful to receive alerts from your server: failed
    updates, manual action required, failing cronjob, ... \\

    There's almost no configuration as debian's package handle basic usage
    out-of-the box.

    \begin{itemize}
      \item exim
      \item postfix
    \end{itemize}
  \end{frame}

  \section{Services}

  \begin{frame}[fragile]
    \frametitle{Your init system: systemd}

    Systemd, widely used init system, is used to manage system services:
    \begin{itemize}
      \item {\texttt systemctl status|start|stop myservice}
      \item {\texttt systemctl enable|disable myservice}
      \item {\texttt journalctl -u myservice}
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{A web server}

    Various options but two major actors: we use them as frontend and to serve
    static content. They 'proxy' to dynamic content.

    \begin{itemize}
      \item apache: older but still widely used.
      \item nginx: newer and with sexier configuration (we'll use this one).
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{A web server}
    \begin{itemize}
      \item {\texttt apt install nginx}
      \item Open firewall on port 80 (HTTP); 443 is for HTTPS.
      \item {\texttt systemctl start nginx}
      \item You should be able to access the default page on your web browser.
    \end{itemize}

  \end{frame}

  \begin{frame}
    \frametitle{A web server}

    \begin{itemize}
      \item All good: we can serve static files.
      \item Nginx's configuration is located in {\texttt /etc/nginx}
        \begin{itemize}
          \item General configuration file: {\texttt /etc/nginx/nginx.conf};
            Debian's default are good enough for now!
          \item The sites's configuration must be put in {\texttt
            /etc/nginx/sites-available}.
          \item To enable a configuration, symlink it from {\texttt
            sites-available} to {\texttt sites-enabled}.
        \end{itemize}
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{A web server}

    How about dynamic content?

    \begin{itemize}
      \item The web server forwards the requests to the application's process.
      \item The application's run under its own user, isolated from other
        processes.
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{A web server}

    You'll see the detailed configuration during live example.

    \begin{itemize}
      \item Install the dependencies: php7.0-fpm, ...
      \item Create a dedicated user: {\texttt useradd -m -d /var/www/dokuwiki -s /bin/false www-dokuwiki}
      \item Configure PHP-FPM (Note: PHP is a bit special)
      \item Configure NGINX to serve this application
    \end{itemize}
  \end{frame}

  \section{More advanced topics}

  \begin{frame}
    \frametitle{A web server}

    HTTPS? You can get free TLS certificates from let's encrypt. There are
    various client, {\texttt dehydrated} works quite well.

    Certificates are specific to (sub)domains. You'll have to add a few lines
    to nginx's configuration and open port 443 in your firewall.
  \end{frame}

  \section{More advanced topics}

  \begin{frame}
    \frametitle{Advanced topic: Monitoring}

    Proper monitoring is essential beyond a trivial setup: you want to be
    notified as soon as possible when things break and be able to monitor
    resource usage.

    \begin{itemize}
      \item icinga2 for service monitoring and alerts
      \item munin for graphs (see munin.gnugen.ch)
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{Advanced topic: Backups}

    You'll want backups if things get serious: shit happens.
  \end{frame}

  \begin{frame}
    \frametitle{Advanced topic: Containers}

    They're everywhere those days. Be careful: they often create as many issues
    as they solve! They can hide anything: as a rule of thumb, do not use them
    unless you know what's going inside and are able to dig in if it becomes
    necessary.
  \end{frame}

  \begin{frame}
    \frametitle{Advanced topic: Authentication}

    When your infrastructure grows beyond a single server, you might want to
    use an unified user account. You can use a directory such as OpenLDAP.
  \end{frame}

  \section{}

  \begin{frame}
    \frametitle{}

    We can live-deploy a new server if you are interested and still have time.
  \end{frame}

  \begin{frame}
    \begin{itemize}
      \item Geekerie on 2019-05-12: more on system administration!
    \end{itemize}

    \begin{multicols}{2}
      \begin{center}
        \includegraphics[width=5cm]{gnugen_logo_officiel.png}
      \end{center}
      \columnbreak
      Libre? Open source? These are our core values at GNU Generation, and what
      we promote on the campus. We are a student club that regularly organizes
      events and offers various services to students and members of the EPFL
      community... As well, of course, as sharing good times at our clubroom!
    \end{multicols}
  \end{frame}
\end{document}
