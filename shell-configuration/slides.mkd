---
title: Shell customization
author: Joachim Desroches
institute: GNU Generation
subject: Computer Science
date: 2018-10-18
...

# Quick reminder: what is a shell and why do I care

## Ask, and you will be answered

* Terminal
* Shell
* Prompt

---

## A happy family

* sh
* bash
* zsh
* fish, csh, tsh, tcsh...

# Options for sh

## Mother of all

 * shopt
   * auto_cd ; correct ; \*glob ; no_beep ;
 * set
   * -e ; -x ; -u
 * rtfm for more!

# Prompt configuration

## A bit of background

* Configuration files
* Environment variables
* RC/Profile/Env

## bash

 * Escape sequences for color and information
 * `PS1='\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] '`
 * Arbitrary commands with `$(foo)`
 * `PS1='[$(battery.sh)] \[\033[01;32m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] '`

## zsh

 * Different syntax, same principles
 * Plugin modules with some cool info (git, ...)
 * Also pre-made prompt themes

# Your screen width is the limit

## Advanced zsh tricks

 * Oh-my-zsh: plugin manager

## Advanced zsh tricks

 * Vim-like navigation / line editing
 * Syntax highlighting
 * Hooks
 * Custom completion...
