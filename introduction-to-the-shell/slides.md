---
title: Introduction to the shell
author: Joachim Desroches
institute: GNU Generation
subject: Computer Science
date: 2019-11-18
...

![Life](computer_problems.png)

# What is a shell and why do I care

## Your computer can have conversations too

* Prompt: `sparrowhawk@roke.island.magic:~ $`
* Commands:

  ~~~~sh
  $ ls; cd; echo "hello world"
  ~~~~

* Shell:

  ~~~~sh
  $ env | grep SHELL
  ~~~~

## History: a Happy Family

* sh
* bash
* zsh, dash, msh, fish, ksh, csh, tsh, tcsh...

# Anatomy of a command

## Context matters

* Environment variables:
  * 
    ~~~~sh
    $ env
    ~~~~

  * 
    ~~~~sh
    $ echo $SHELL
    ~~~~

* Filesystem location:

  ~~~~sh
  $ pwd
  ~~~~

---

## Context matters

* Filesystem sidetrack
  - Absolute: `/home/sparrowhawk/ilovebsd`
  - Relative from `/home`: `sparrowhawk/ilovebsd`
  - Relative from `/usr/`: `../home/sparrowhawk/ilovebsd`

---

## Context matters



> * Environment variables:

  ~~~~sh
  $ env
  ~~~~

> * Filesystem location:

  ~~~~sh
  $ pwd
  ~~~~

## Arguments and feedback

* Commands may take arguments.
  * Short form (one dash, one letter)

    ~~~~sh
    $ ls -larh
    ~~~~

  * Long form (two dashes)

    ~~~~sh
    $ ls --long --all --reverse --human
    ~~~~

* Commands may or may not print feedback
  * Print errors:

    ~~~~sh
    $ rm doesnotexist
    ~~~~

  * Return codes:

    ~~~~sh
    $ echo $?
    ~~~~

# Scripting

## Being lazy

* What is a script ?
* Avoid repetitive tasks
* Shebang: `#!/path/to/my/shell`
  * 
    ~~~~sh
    #!/bin/sh
    ~~~~

  * 
    ~~~~sh
    #!/usr/bin/python
    ~~~~

  * 
    ~~~~sh
    #!/usr/bin/perl
    ~~~~

* Remember: context!
* First example: `xvim`

## Syntax

### Chaining commands

~~~sh
echo "hello" && echo "world"
~~~

~~~~sh
echo "hello" || echo "goodbye"
~~~~

~~~~sh
echo "hello" | ls
~~~~

### Conditionals

~~~~sh
if [ $cond ];
then
	$code;
else
	$code;
fi
~~~~

---

## Syntax

### Iteration

~~~~sh
for $var in $elems;
do
	$code;
done
~~~~

### Loop

~~~~sh
while [ $cond ];
do
	$code;
done
~~~~

# Final thoughts

## Script the interpeter

Above control structures work in the interactive shell too!

~~~~sh
$ for $f in $(ls); do mv $f{,.bak}; done
~~~~

## Important variables

:::::::::::::: {.columns}
::: {.column width="30%"}

* `$SHLVL`
* `$PATH`
* `$TERM`

:::
::: {.column width="30%"}

* `$HOME`
* `$EDITOR`
* `$PAGER`

:::
::: {.column width="30%"}

* `FOO=bar` (no spaces!)
* `export FOO` (change env)

:::
::::::::::::::

## Read The Fine Manual

`man man`

## Check your scripts

Checkout the `shellcheck` tool. It's awesome!

~~~~sh
In makesubmission.sh line 7:
cd client
^-------^ SC2164:
Use 'cd ... || exit' or 'cd ... || return' in case cd fails.

Did you mean: 
cd client || exit

For more information:
  https://www.shellcheck.net/wiki/SC2164
~~~~

