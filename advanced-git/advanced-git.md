---
title: Advanced Git
author: Roosembert Palacios
date: March 6, 2019
license: CC BY-SA

height: 850
---

### Advanced git: <br>commit, rebase and merge with a stop at the reflog

Slides: https://orbstheorem.ch/talks/intermediate-git-0319/talk.html  
Pdf: https://orbstheorem.ch/talks/intermediate-git-0319/talk.pdf  
Git: https://gitlab.gnugen.ch/gnugen/presentations  

---

# Overview
- Summary of basic commands
  - pull, fetch, add, commit
  - merge (fast forward)
  - Typical Git usage

- Advanced usage of “git commit”
  - amend, fixup, squash

- Rewriting history tools
  - Orphan branches
  - Empty git commits

- Git rebase
  - Applying the fixup and squash commits
  - interactive, `--auto-awesome`

# Let's get started!

---

## Summary of basic commands
Git is a distributed version control system.

- __distributed__
- __version control__

---

### Simple usage
Add file contents to the index
```shell
~$ echo "Hello world" > file.txt
~$ git add file.txt
~$ git commit -m "New nice little file"
[master (root-commit) e9b85bc] New nice little file
 1 file changed, 1 insertion(+)
 create mode 100644 file.txt
~$ git push origin master
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Writing objects: 100% (3/3), 237 bytes | 237.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To /run/user/1000/tmp.GsvjXxf0hw/
 * [new branch]      master -> master
```

---

### Receiving changes
```shell
~$ git pull
From /run/user/1000/tmp.GsvjXxf0hw
   4587305..af51a1f  master     -> origin/master
Updating 4587305..af51a1f
Fast-forward
 samfile | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 samfile
```

```shell
~$ git fetch origin
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 4 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (4/4), done.
From /run/user/1000/tmp.GsvjXxf0hw
   4587305..af51a1f  master     -> origin/master
~$ git merge origin/master
Updating 4587305..af51a1f
Fast-forward
 samfile | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 samfile
```
---

### pull vs fetch + merge
- _git pull_ is (basically) just _git fetch_ + _git merge_
- Why not simply use git pull then?
  ```shell
  ~$ date > file2.txt
  ~$ git add file2.txt
  ~$ git commit -m "Another nice little file"
  [master 4bfc288] Another nice little file
   1 file changed, 1 insertion(+)
   create mode 100644 file2.txt
  ~$ git pull
  From /run/user/1000/tmp.GsvjXxf0hw
     4587305..af51a1f  master     -> origin/master
  Merge made by the 'recursive' strategy.
   samfile | 1 +
   1 file changed, 1 insertion(+)
   create mode 100644 samfile
  ```

---

### pull vs fetch + merge (cont.)

- Merge spaguetti  
  ![](./01-merge-spaguetti.png)

---

### pull vs fetch + merge (cont.)
- Still not convinced?  
  ![](./01-merge-my-eyes.png)

---

### pull vs fetch + merge (cont.)
- Hopefully I convinced you
- Refuse merges, rebase instead!
  ```shell
  ~$ git pull origin master --ff-only
  remote: Enumerating objects: 4, done.
  remote: Counting objects: 100% (4/4), done.
  remote: Compressing objects: 100% (3/3), done.
  remote: Total 3 (delta 0), reused 0 (delta 0)
  Unpacking objects: 100% (3/3), done.
  From /run/user/1000/tmp.GsvjXxf0hw
   * branch            master     -> FETCH_HEAD
   + e708300...4d8b99f master     -> origin/master  (forced update)
  fatal: Not possible to fast-forward, aborting.
  ~$ git rebase origin/master
  First, rewinding head to replay your work on top of it...
  Applying: Another nice little file
  ```

---

### pull vs fetch + merge (cont.)
- Semilinear history
  ```shell
  ~$ git merge --ff-only --no-ff feat/another_feature
  Merge made by the 'recursive' strategy.
   file2.txt | 1 +
   1 file changed, 1 insertion(+)
   create mode 100644 file2.txt
  ~$ git push origin master
  ```
  ![](./01-semilinear-history.png)
  “Safe checkpoints” where code is supposed to work.  
  Such checkpoints are usefull for tools like git-bisect

---

### pull vs fetch + merge (cont.)
- Easier to understand  
  ![](./01-beautiful-history.png)

---

### (Simplified) Typical Git usage
- Implement new feature
  ```shell
  ~$ git checkout -b feat/local_database
  Switched to a new branch 'feat/local_database'
  ~$ work work work > newfeature
  ~$ git add newfeature
  ~$ git commit -svS -m "Some serious work over here"
  [feat/local_database 608bbb1] Some serious work over here
   1 file changed, 3 insertions(+)
   create mode 100644 newfeature
  ```

- Push, open merge request  
  ![](./01-new-feature-branch.png)

- Repeat

---

### (Real-life) Typical Git usage
- Implement new feature
- Push, open merge request as wip (work in progress)
- Repeat many times:
  - Mess up
  - Hate your CI provider _(\*cough\* Travis \*cough\*)_
  - Try to fix you mess
- Your changes are approved and all tests pass!  
  ![](./01-reallife-sweng.png)

---

## Intermediate usage of <br>“git commit”

Let's start with some definitions!

---

### Definition: A Git commit
- A commit is roughly composed by
  - The hash of the parent commit
  - The diff of the commit (file difference between the old version of the files and the new one)
  - The authorship and commiter data
  - A hash of all the previous fields, which is used to identify the commit
- This implies that, if we change the order, the hash of the parent commit will change, therefore the hash of the commit itself will change!

---

### Definition: HEAD
- HEAD is a _dynamic reference_ to the latest commit of your current branch
- Changing branches , also changes the value of HEAD

---

### Definition: tree
- A tree is _a reference to a commit_
- Since commits include the hash of their parent, we can build a linear history tracing up to the root commit of the branch

---

### Definition: branch
- A tree with a name
- Running `git commit` within a branch, updates the reference of the branch

---

### Definition: cherry-picking
- Cherry-picking is applying a commit from a different tree on top of HEAD
- Cherry-picking works by taking a commit's diff and _applying it on top of a branch_.  
The parent commit's hash has no role in cherry-picking.  
For instance cherry-picking f33f475 would produce a conflict, because said diff is already applied by 101cc1e.


---

### Intermediate usage of “git commit”
- Say we'd like to modify something in our last commit  
  ![](./02-amend-finally-1.png)

---

### Intermediate usage of “git commit”
- Do your modifications
- __Amend__ your latest commit  
  ![](./02-amend-finally-2.png)

---

### Intermediate usage of “git commit”
- What if I want to modify an older commit?
  - `git commit --fixup`  
    This modification is minor (e.g. a typo)

  - `git commit --squash`  
    This modification is relevant, I'd like to be reformulate my commit

---

### Intermediate usage of “git commit”
![](./02-squash-before-rebase.png)

---

### Intermediate usage of “git commit”
- _I don't want a new commit, I want to fix that previous commit_

---

### Intermediate usage of “git commit”
- _I don't want a new commit, I want to fix that previous commit_
- Rebase your branch and apply fixup and squash commits (more on this later)

---

### Rewriting history tools
- `git checkout --orphan newbranch`
  Creates an orphan branch, with no parent commit  
  ![](./02-checkout-orphan-branch.png)

---

### Ministry of thruth (cont.)
- Creating empty commits  
  `git commit --allow-empty`  
  ![](./02-empty-commit-1.png)
  ![](./02-empty-commit-2-tig.png)

---

### Rewriting history (cont.)
- Why create empty commits?  
  Rebasing __requires a parent commit__. If you mess up your first commit, you mess up your branch line

- Why create orphan branches?  
  - To fix your root commit (see abode)
  - Advanced subtree merging
  - Publishing a commit's history tree without exposing its full history
  - [Combining multiple repositories][1]
  - Cross-repository cherry-picking

[1]: http://www.gelato.unsw.edu.au/archives/git/0506/5511.html

---

### Doctoring your commit history
- Motivation: No one should ever merge this to prod:  
  ![](./02-dirty-history.png)
  - Development is never linear, you can't plan your bugs! History rewrite to the rescue!

---

### Applying fixup and squash commits
- __After__ the fixup/squash commit, we need to rebase
  ```shell
  ~$ git rebase --interactive --autosquash --autostash origin/master
  ```
  squash and fixups will be automatically applied on top of their related commit  
  ![](./02-rebase-in-progress-1.png)

---

### Applying fixup and squash commits
  ![](./02-rebase-in-progress-2-squash.png)
  ![](./02-rebase-end.png)

---

#### Interactive rebase
```shell
~$ git rebase --interactive --autosquash --autostash origin/master
```
- autostash:
    - save uncommitted modifications before rebase
    - applies back those changes when the rebase has finish

---

#### Interactive rebase
```shell
~$ git rebase --interactive --autosquash --autostash origin/master
```
- interactive: opens a todo-list where you can reorder, edit, reword, fixup,
  squash, drop, __insert!__ (_cherry-pick_) commits

---

#### Interactive rebase
```shell
~$ git rebase --interactive --autosquash --autostash origin/master
```
- autosquash flag inserts fixup and squash commits after their references.  
  It also applies the squash or fixup of the commit on top

---

#### Interactive rebase
```shell
~$ git rebase --interactive --autosquash --autostash origin/master
```
- origin/master is the “rebase base”, this may be a child of a parent commit or a parent commit itself

---

### Rewriting history (cont.)
#### Interactive rebase
```shell
~$ git rebase --interactive --autosquash --autostash origin/master
```
![](./02-dirty-history-reordering.png)

---

### Rewriting history (cont.)
#### When rebasing goes south
Suppose you want to remove the unnecessary file before adding your 3 new files:  
![](./03-reorder-me.png)
But you mistype and you end up removing an interesting commit  
![](./03-lost-a-commit.png)

---

### Rewriting history (cont.)
#### git reflog
This command is useful to manipulate the reference logs, we'll take a look at the `show` subcommand. Others are available but their use goes well beyond an intermediate course.

---

### Rewriting history (cont.)
#### git log vs git reflog
- Git log shows the history of the tree
- Git reglog shows the history of HEAD, which “jumps” every time you checkout a branch, make a commit.

---

#### git reflog (cont.)
The reflog shows the “history” of HEAD in rev. order. The first line is the current HEAD (latest commit).  
![](./03-fishing-in-the-reflog.png)
You can see our latest rebase on the 5th line. Since then we've evolved our branch through commits b5c20a5, 101cc1e, 429cd8

---

#### git reflog (cont.)
![](./03-fishing-in-the-reflog.png)
We can see our previous commits before HEAD@{4}.  
In HEAD@{4}, HEAD was rewinded back to the rebase base (i.e. origin/master) and some commits were (cherry-)picked on top.

---

#### git reflog (cont.)
![](./03-fishing-in-the-reflog.png)
We can see that commit (Remove unnecessary file)'s hash evolved from 4a74672 to b5c20a5.  
Indeed its parent was previously f02bdd7 and now is master (526c2a0)

---

#### git reflog (cont.)
![](./03-fishing-in-the-reflog.png)
Likewise commit (f1)'s hash evolved from f33f475 to 101cc1e. Its parent was master (526c2a0) and now is b5c20a5
---

#### git reflog (cont.)
![](./03-fishing-in-the-reflog.png)
However, we can cherry-pick a328d2e, which contains the diff for f2. The commit we're interested on.  

---

#### git reflog (cont.)
We can either `git cherry-pick a328d2e`  
or just add `pick a328d2e` in git rebase -i:  
![](./03-cherry-picking-into-rebase.png)

---

#### git reflog (cont.)
a328d2e evolved into a851abf and was applied on top of 101cc1e (as expected).  
429c4d8 evolved to cf4225a, because its parent changed from 101cc1e to a851abf (as expected).  
![](./03-rebase-done.png)

---

# QA
