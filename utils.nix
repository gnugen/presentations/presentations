{ lib, stdenv, cmake, zlib, ncurses, python
}:

{
  all-build-tools = [];

  copySource = name: sourcedir:
    stdenv.mkDerivation {
      name = name;
      src = pkgs.lib.cleanSource ./.;
      buildCommand = ''
        mkdir $out
        cp -r -a $src/* $out/
      '';
    };

  
}

#stdenv.mkDerivation {
#  name = "unvanquished-nacl-vms";
#  src = source;
#
#  preConfigure = ''
#    rm -r daemon
#    cp -r ${daemon-source} daemon
#    chmod +w daemon -R
#
#    mkdir daemon/external_deps/linux64-${nacl-hacks.binary-deps-version}/
#    cp ${nacl-hacks.unvanquished-binary-deps}/* daemon/external_deps/linux64-${nacl-hacks.binary-deps-version} -r
#    chmod +w -R daemon/external_deps/linux64-${nacl-hacks.binary-deps-version}/
#
#    #FIXME: remove as this is duplicated with nacl-hacks
#    interpreter="$(< "$NIX_CC/nix-support/dynamic-linker")"
#    for f in /build/source/daemon/external_deps/linux64-5/pnacl/bin/*; do
#      if [ -f "$f" ] && [ -x "$f" ]; then
#        echo "Patching $f"
#        patchelf --set-interpreter "$interpreter" "$f" || true
#      fi
#    done
#  '';
#
#  nativeBuildInputs = [
#    cmake
#    nacl-hacks.unvanquished-binary-deps
#    (python.withPackages (ppkgs: [ppkgs.jinja2 ppkgs.pyyaml]))
#  ];
#  buildInputs = [
#    zlib
#    ncurses
#  ];
#
#  cmakeFlags = [
#    "-DBUILD_CLIENT=NO"
#    "-DBUILD_SERVER=NO"
#    "-DBUILD_TTY_CLIENT=NO"
#    "-DBUILD_CGAME=YES"
#    "-DBUILD_SGAME=YES"
#    "-DBUILD_GAME_NACL=YES"
#    "-DBUILD_GAME_NATIVE_EXE=NO"
#    "-DBUILD_GAME_NATIVE_DLL=NO"
#    "-DUSE_HARDENING=TRUE" # probably useless
#    "-DUSE_LTO=TRUE"       # probably useless
#  ];
#
#  installPhase = ''
#    install -Dm0644 sgame-x86_64-stripped.nexe $out/sgame-x86_64.nexe
#    install -Dm0644 sgame-x86-stripped.nexe    $out/sgame-x86.nexe
#    install -Dm0644 cgame-x86_64-stripped.nexe $out/cgame-x86_64.nexe
#    install -Dm0644 cgame-x86-stripped.nexe    $out/cgame-x86.nexe
#  '';
#}
