#!/bin/bash
#
# File: /usr/local/bin/enableprinters.sh
# Extract all printernames from the PRINTCAP file and enable all printers.
# This is to restart stalled printers without manual action.
# Specially useful for Remote Printing on printcatxxx servers
#
# To be put as a cron job (user: root)
#

PRINTCAP=/etc/cups/printcap
LOGFILE=/var/spool/remoteprint/printerstatus
# Make sure the PATH is right
PATH=/bin:/usr/bin:$PATH

#
# END OF USER-CONFIGURABLE AREA
#
# Maintain a log file history
#
mv $LOGFILE\19 $LOGFILE\20
mv $LOGFILE\18 $LOGFILE\19
mv $LOGFILE\17 $LOGFILE\18
mv $LOGFILE\16 $LOGFILE\17
mv $LOGFILE\15 $LOGFILE\16
mv $LOGFILE\14 $LOGFILE\15
mv $LOGFILE\13 $LOGFILE\14
mv $LOGFILE\12 $LOGFILE\13
mv $LOGFILE\11 $LOGFILE\12
mv $LOGFILE\10 $LOGFILE\11
mv $LOGFILE\09 $LOGFILE\10
mv $LOGFILE\08 $LOGFILE\09
mv $LOGFILE\07 $LOGFILE\08
mv $LOGFILE\06 $LOGFILE\07
mv $LOGFILE\05 $LOGFILE\06
mv $LOGFILE\04 $LOGFILE\05
mv $LOGFILE\03 $LOGFILE\04
mv $LOGFILE\02 $LOGFILE\03
mv $LOGFILE\01 $LOGFILE\02
mv $LOGFILE $LOGFILE\01

echo "********************************************************" > $LOGFILE
echo "*              Actual Printer Status                   *" >> $LOGFILE
date  >> $LOGFILE
echo "********************************************************" >> $LOGFILE
lpstat -t >> $LOGFILE
echo >> $LOGFILE

#
# And now,for something completely different, ahem.., cryptic, rather:
#
for PRINTER in `grep -v "#" $PRINTCAP | awk '{print substr($1,1,index($1,"|")-1)}'`;
	do /usr/bin/enable $PRINTER;
done

echo >> $LOGFILE
echo "********************************************************" >> $LOGFILE
echo "*              After Enable-Script Run                 *" >> $LOGFILE
date  >> $LOGFILE
echo "********************************************************" >> $LOGFILE
lpstat -t >> $LOGFILE
echo >> $LOGFILE
