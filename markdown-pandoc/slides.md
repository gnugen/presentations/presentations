---
title: Introduction à Markdown
author: David Dervishi
date: 2022-02-22
hyperlinks: true
theme: "Frankfurt"
colortheme: "beaver"
fonttheme: "professionalfonts"
mainfont: "Hack Nerd Font"
fontsize: 11pt
urlcolor: red
linkstyle: bold
aspectratio: 169
titlegraphic: gnugen.png
toc: false
header-includes: |
    ```{=latex}
    \setbeamertemplate{blocks}[rounded][shadow=false]
    ```
---

# Markdown

 * Langage de composition de documents
 * Syntaxe légère et lisible
 * Ecriture simple et rapide

# Pandoc

 * Convertisseur de documents
 * Markdown, HTML, \LaTeX{}, PDF, ODT, ...

## Installation

    # apt install pandoc
    # pacman -S pandoc

Ou sur le [site de Pandoc](https://pandoc.org)

\pause

## Utilisation

    $ pandoc -s -o <output> <input>
    $ pandoc -s -o slides.pdf slides.md
    $ pandoc -s --from markdown --to pdf -o slides.pdf slides.md
    $ pandoc -s --from markdown+fancy_lists -o slides.pdf slides.md

# Document simple

## Métadonnées

 * Titre, date, thème, options, ...

```markdown
---
title: My Nice Document
subtitle: Your Nice Subtitle
author:
  - Your Name
  - Name Of Teammate
date: 2022-02-22
numbersections: true
---
```

***

## Contenu

 * Sections, paragraphes, ...

```markdown
# Section

Lorem ipsum dolor sit amet

## Sous-section

consectetur adipiscing elit

### Sous-sous-section

sed do eiusmod tempor incididunt

ut labore et dolore magna aliqua
```

***

![](structure.png)

# Listes

    * Item 1
    * Item 2
      + Item 2.1
      + Item 2.2
    * Item 3

\pause

 * Item 1
 * Item 2
   + Item 2.1
   + Item 2.2
 * Item 3

# Enumérations

    1. 1
    2. 2
       i. i
       ii. ii
    3. 3

\pause

 1. 1
 2. 2
    i. i
    ii. ii
 3. 3

# Enumérations automatiques

    #. Alice
    #. Bob
       #. Charlie
       #. Dave
    #. Eve

 #. Alice
 #. Bob
    #. Charlie
    #. Dave
 #. Eve

# Formattage

 - `*emphase*, _emphase_`: *emphase*, _emphase_
 - `**beaucoup** __d'emphase__`: **beaucoup** __d'emphase__
 - `~~tracer~~`: ~~tracer~~
 - `des^exposants^ et des~indices~`: des^exposants^ et des~indices~
 - `` `monospace` ``: `monospace`
 
# Liens et références

    [nom](lien)
    [GNU Generation](https://gnugen.ch)

[GNU Generation](https://gnugen.ch)

\pause

    [lien interne](#nom-de-section)
    [Section précédente](#formattage)

[Section précédente](#formattage)

\pause

    Note[^label]
    
    [^label]: Ceci est une note

Note[^label]

[^label]: Ceci est une note

# Images

    ![caption](link)

![Markdown logo](https://raw.githubusercontent.com/dcurtis/markdown-mark/master/png/208x128.png)

# Code

```markdown
    indentation de 4 espaces
```

\pause

````markdown
```langage
code
```
````

\pause

````markdown
```{=lang}
code qui va directement être passé à <lang>
```
````

# Maths et \LaTeX{}

Directement dans le code Markdown

```latex
\begin{align*}
    x + 2 &= 0 \\
    x &= -2
\end{align*}
```

\pause

\begin{align*}
    x + 2 &= 0 \\
    x &= -2
\end{align*}

# Tables

    | Product  | Cost | Quantity | Total |
    |:---------|-----:|---------:|------:|
    | Apple    | 5    | 4        | 20    |
    | Broccoli | 2    | 8        | 16    |
    | Chili    | 2    | 5        | 10    |
    | Date     | 3    | 5        | 15    |
    |          |      |          | 61    |

    : Example 1

***

| Product  | Cost | Quantity | Total |
|:---------|-----:|---------:|------:|
| Apple    | 5    | 4        | 20    |
| Broccoli | 2    | 8        | 16    |
| Chili    | 2    | 5        | 10    |
| Date     | 3    | 5        | 15    |
|          |      |          | 61    |

: Example 1

***

    -------------------------------
    Product   Cost  Quantity  Total
    -------- ----- --------- ------
    Apple        5         4     20

    Broccoli     2         8     16

    Chili        2         5     10

    Date         3         5     15

                                 61
    -------------------------------

    : Example 2

***

-------------------------------
Product   Cost  Quantity  Total
-------- ----- --------- ------
Apple        5         4     20

Broccoli     2         8     16

Chili        2         5     10

Date         3         5     15

                             61
-------------------------------

: Example 2

***

    \begin{table}[ht]
        \centering
        \caption{Example 3}
        \begin{tabular}{|l|r|r|r|}
            \hline Product  & Cost & Quantity & Total \\
            \hline Apple    & 5    & 4        & 20    \\
                   Broccoli & 2    & 8        & 16    \\
                   Chili    & 2    & 5        & 10    \\
                   Date     & 3    & 5        & 15    \\
            \hline          &      &          & 61    \\
            \hline
        \end{tabular}
    \end{table}

***

\begin{table}[ht]
    \centering
    \caption{Example 3}
    \begin{tabular}{|l|r|r|r|}
        \hline Product  & Cost & Quantity & Total \\
        \hline Apple    & 5    & 4        & 20    \\
               Broccoli & 2    & 8        & 16    \\
               Chili    & 2    & 5        & 10    \\
               Date     & 3    & 5        & 15    \\
        \hline          &      &          & 61    \\
        \hline
    \end{tabular}
\end{table}

<!-- SPDX-License-Identifier: CC-BY-SA-4.0 -->
