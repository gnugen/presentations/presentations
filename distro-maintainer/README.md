These slides are made with the "showoff" tool. The "static" directory contains
precompiled slides.

If you wish to update them, you can compile them with `showoff static`.
`showoff serve` can also prove useful for live presentations.
