<!SLIDE incremental>
# *What* is a Linux distribution

* A Linux distribution is what you install when you "install linux" ![Distros](../_images/distros_logo.png)

* A Linux distribution is a collection of software, carefully arranged together

<!--
* Sometimes you have a Stable, Edge or LTS release

* Sometimes you have variants with various graphical enviroments, or lack thereof
-->

<!SLIDE incremental>
# *What* is a package

* A package is an archive (a zip file) that contains a piece of software
  alongside some installation instructions

* You've got a package for everything
	* the Linux kernel,
	* the C standard library,
	* Firefox,
	* the GNOME Text Editor,
	* each web server,
	* many, many more.

* those packages have dependencies
	* installing `firefox` will install `fontconfig`, `libx11`,
	  `libdbus`, `libgtk3` and many more.

* there are "meta" packages that can install a collection of packages
	* `libreoffice` install `libreoffice-draw`,
	  `libreoffice-writer`, and all the other tools of the suite.

<!--
* there are "virtual" packages that represents alternatives
	* installing `linux-image-generic` will install one version of the
	  linux kernel
-->


<!SLIDE incremental>
# *What* is a distro maintainer

There are two kind of distributions. The ones that are made by companies,
and the ones who are made by a community of individuals. We will only talk
about the latter here.

* A Linux distribution maintainer is someone who helps making the distribution

* A maintainer or a team of maintainer is usually responsible for a part of the
  system
	* It can be a single package ("Firefox maintainer")
	* It can be a collection of software ("GNOME Packaging Team")
	* It can be some aspect of the software ("Security Team")
