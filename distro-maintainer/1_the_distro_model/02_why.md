<!SLIDE incremental>
# *Why* are there Linux distributions

* Distributions collect some of the many, many pieces of free software out there

* Distributions are making them integrate nicely with each another

* Distributions carry the infrastructure needed
	* a package repository
	* a bugtracker
	* some mean of internet messaging for developers and community
	* some installer

* Distributions promote and defend free software

* Distributions have security policies

* Distributions *set* boundaries


<!SLIDE incremental>
# *Why* is a distro maintainer here

* They add new software

* They will adapt the software if need be

* They keep packages up to date

* They fix bugs

* They are the one to *enforce* the boundaries.
