<!SLIDE incremental>
# *How* do we make Linux distributions

* Package some software

* Upstream patches of the software you modified

<!-- * Continuous integration -->

* Distribution lifecycle


<!SLIDE incremental>
# *How* can one help and maybe become a distro maintainer

* Report bugs

* Test and trial software

* Package software you use

* Help keeping the infrastructure working

* Donate

* Did I say report bugs?
